﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace ListViewLayoutProblem
{
    internal class SampleAdapter : BaseAdapter<SampleItem>
    {
        Context context;
        private MainActivity mainActivity;
        private Collection<SampleItem> items;
        Dictionary<SampleItem, SampleItemViewBinder> binders = new Dictionary<SampleItem, SampleItemViewBinder>();

        public SampleAdapter(Context context, Collection<SampleItem> items)
        {
            this.context = context;
            this.items = items;
        }

        public override SampleItem this[int position] => items[position];

        public override int Count => items.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            var view = convertView;

            if (view == null)
            {
                view = (context as Activity).LayoutInflater.Inflate(Resource.Layout.ListViewItem, null);
            }

            SampleItemViewBinder binder = new SampleItemViewBinder(item, view);
            if (binders.ContainsKey(item))
            {
                var binderToRemove = binders[item];
                binderToRemove.Release();
                binders.Remove(item);
            }
            binders.Add(item, binder);

            return view;


        }
    }

    internal class SampleItemViewBinder
    {
        private SampleItem item;
        private View view;

        public SampleItemViewBinder(SampleItem item, View view)
        {
            this.item = item;
            this.view = view;

            item.PropertyChanged += Item_PropertyChanged;

            var tv= view.FindViewById<TextView>(Resource.Id.myText);
            tv.Text = item.MyText;
        }

        private void Item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var tv = view.FindViewById<TextView>(Resource.Id.myText);
            tv.Text = item.MyText;
        }

        internal void Release()
        {
            if (item != null)
                item.PropertyChanged -= Item_PropertyChanged;
        }
    }
}