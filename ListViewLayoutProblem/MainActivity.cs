﻿using Android.App;
using Android.Widget;
using Android.OS;

using System.Threading;
using System.ComponentModel;

namespace ListViewLayoutProblem
{
    class SampleItem : INotifyPropertyChanged
    {
        private string myText;

        public string MyText
        {
            

            get
            {
                return myText;
            }

            set
            {
                myText = value;
                OnPropertyChanged(nameof(MyText));
            }

        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public event PropertyChangedEventHandler PropertyChanged;
    };

    [Activity(Label = "ListViewLayoutProblem", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;
        

        ObservableCollectionEx<SampleItem> items = new ObservableCollectionEx<SampleItem>();
        private SampleAdapter adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            items.Add(new SampleItem() { MyText = "???" });

            // Get our button from the layout resource,
            // and attach an event to it
            ListView lv = FindViewById<ListView>(Resource.Id.myListView);
            adapter = new SampleAdapter(this, items);
            lv.Adapter = adapter;
            items.CollectionChanged += (sender, args) =>
            {
                adapter.NotifyDataSetChanged();
            };

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                int i = 0;

                while (true)
                {
                    this.RunOnUiThread(delegate
                    {
                        items[0].MyText = $"Counter = {i}";
                    });

                    Thread.Sleep(1000);
                    i++;
                }
            }).Start();
        }
    }
}

