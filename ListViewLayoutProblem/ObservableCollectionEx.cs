﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ListViewLayoutProblem
{
    public class ObservableCollectionEx<T> : ObservableCollection<T> where T : INotifyPropertyChanged
    {
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            Unsubscribe(e.OldItems);
            Subscribe(e.NewItems);
            base.OnCollectionChanged(e);
        }

        protected override void ClearItems()
        {
            foreach (T element in this)
                element.PropertyChanged -= OnContainedElementChanged;

            base.ClearItems();
        }

        private void Subscribe(IList iList)
        {
            if (iList != null)
            {
                foreach (T element in iList)
                    element.PropertyChanged += OnContainedElementChanged;
            }
        }

        private void Unsubscribe(IList iList)
        {
            if (iList != null)
            {
                foreach (T element in iList)
                    element.PropertyChanged -= OnContainedElementChanged;
            }
        }

        private void OnContainedElementChanged(object sender, PropertyChangedEventArgs e)
        {
            var ex = new PropertyChangedEventArgsEx(e.PropertyName, sender);
            OnPropertyChanged(ex);
            ContainedElementChanged?.Invoke(sender, e.PropertyName);
        }

        public virtual event ContainedElementChangedEventHandler ContainedElementChanged;

        public delegate void ContainedElementChangedEventHandler(object item, string property);
    }

    public class PropertyChangedEventArgsEx : PropertyChangedEventArgs
    {
        public object Sender { get; private set; }

        public PropertyChangedEventArgsEx(string propertyName, object sender)
            : base(propertyName)
        {
            this.Sender = sender;
        }
    }
}
